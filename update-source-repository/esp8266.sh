#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Script pulls and latest version of the MicroPython project for the ESP8266 
#                   platform and builds an necessary libraries.
# 
# @note             Errors during Git updates will be presented as a warning to confirm but will not 
#                   break compiling [done this way to mitigate issues with local customizations].
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - make
#                   - git
# 
# @version          1.0.0
# 
# @retval           0                       Successfully updated the ESP open SDK utility.
# @retval           1                       Unable to build cross-compiler.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################
# Update path to include ESP open SDK.
export PATH="${DIR_TC_ESP_OPEN_SDK}/xtensa-lx106-elf/bin:${PATH}"

# Change to project root directory.
cd ${DIR_SOURCE_ESP8266}

# Update and synchronize git repository.
git pull
rc=$?
if [ ${rc} -eq 0 ]; then
    git submodule update --init
    rc=$?
    if ! [ ${rc} -eq 0 ]; then
        prompt_with_warning "Unable to execute Git submodule update [rc == ${rc}]."
    else
        echo "Git repository successfully updated. Proceeding with build."
    fi
else
    prompt_with_warning "Unable to execute Git pull [rc == ${rc}]."
fi

# Update cross-compiler.
make -C mpy-cross
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 1 "Error building MicroPython cross-compiler."
else
    echo "MicroPython cross-compiler successfully built." 
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "All build tasks completed successfully."


