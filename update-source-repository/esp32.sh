#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Script pulls and latest version of the MicroPython project for the ESP32 
#                   platform and builds an necessary libraries.
# 
# @note             Errors during Git updates will be presented as a warning to confirm but will not 
#                   break compiling [done this way to mitigate issues with local customizations].
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - make
#                   - git
# 
# @version          1.0.0
# 
# @retval           0                       Successfully updated the ESP open SDK utility.
# @retval           1                       Unable to build cross-compiler.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################
# Update path to include ESP open SDK.
export PATH="${DIR_TC_XTENSA_ESP32}/builds/xtensa-esp32-elf/bin:${PATH}"
export ESPIDF="${DIR_TC_ESP_IDF}"

# Change to project root directory.
cd ${DIR_SOURCE_ESP32}

# Update main source git repository.
git pull
rc=$?
if [ ${rc} -eq 0 ]; then
    git submodule update --init
    rc=$?
    if ! [ ${rc} -eq 0 ]; then
        prompt_with_warning "Unable to execute Git submodule update [rc == ${rc}]."
    else
        echo "Git repository successfully updated. Proceeding with build."
    fi
else
    prompt_with_warning "Unable to execute Git pull [rc == ${rc}]."
fi

# Update external 'ESP-IDF' toolchain if the SHA-1 dependency is:  (1) located; (2) different than 
# the current SHA-1 for the 'ESP-IDF' toolchain.
fpath_makefile="$(pwd)/ports/esp32/Makefile"
if [ -e "${fpath_makefile}" ]; then
    
    sha_crnt=$(git -C "${DIR_TC_ESP_IDF}" show -s --pretty=format:'%H')
    sha_rqd="$(cat ${fpath_makefile} | grep -i "espidf_suphash" | grep ":=" | cut -d'=' -f2 | awk '{$1=$1}1')"
    
    if [ "${sha_crnt}" == "" ] || [ "${sha_rqd}" == ""]; then
        prompt_with_warning "Unable to discern if the ESP-IDF toolchain is at the appropriate point. This may result in dependency bugs."
    
    elif [ "${sha_crnt}" == "${sha_rqd}" ]; then
        echo "Confirmed the ESP-IDF toolchain is at the appropriate point for the current target build."
    
    else
        echo "Attempting to update the ESP-IDF toolchain from '${sha_crnt}' to '${sha_rqd}' before building."
        
        # Change to the ESP-IDF install directory.
        dir_crnt="$(pwd)"
        cd "${DIR_TC_ESP_IDF}"
        
        # Attempt to switch back to master, fetch all branches, and then pull.
        flag_git_success=0
        git checkout master && git fetch && git pull
        if ! [ $? -eq 0 ]; then
            prompt_with_warning "Failed to update ESP-IDF repository. Will still attempt to find required SHA-1 dependency."
        fi
        
        # Attempt to switch branches.
        branch_name="SHA-${sha_rqd}"
        if [ "$(git branch -v | awk '{$1=$1}1' | grep "^${branch_name}")" != "" ]; then
            git checkout "${branch_name}"
        else
            git checkout "${sha_rqd}" -b "${branch_name}"
        fi
        
        # Verify at correct SHA-1.
        if ! [ $? -eq 0 ] || [ "$(git -C "${DIR_TC_ESP_IDF}" show -s --pretty=format:'%H')" != "${sha_rqd}" ]; then
            prompt_with_warning "Failed to reach desired SHA-1 on the ESP-IDF repository. This may result in dependency bugs."
        fi
        
        # Synchronize Git submodules.
        git submodule update --init
        if ! [ $? -eq 0 ]; then
            prompt_with_warning "Unable to execute Git submodule update. This may result in dependency bugs."
        else
            echo "Successfully updated ESP-IDF repository to the proper SHA-1."
        fi
        
        # Return to original directory.
        cd "${dir_crnt}"
        
    fi
else
    prompt_with_warning "Unable to discern makefile location after updating build target Git repository. This may result in dependency bugs for the ESP-IDF toolchain."
fi




# Update cross-compiler.
make -C mpy-cross
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 1 "Error building MicroPython cross-compiler."
else
    echo "MicroPython cross-compiler successfully built." 
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "All build tasks completed successfully."


