#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Library file providing BASH helper functions and constants available to all 
#                   MicroPython utility scripts.
# 
# @details          Defines ESP tool related constants and wrapper functions.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
# 
# @version          1.0.0
####################################################################################################


####################################################################################################
# @details          Define file-specific constants.
# 
# @warning          While these constants are used in this file, take care to not be too generic in 
#                   your naming convention as all library files will be pulled into the active 
#                   envrionment when the 'lib.sh' file is sourced.
####################################################################################################
ESP_TOOL="/home/sandbox/.local/lib/python2.7/site-packages/esptool.py"          # Path to ESP tool used to interact with flash memory on ESP devices.
ESP_TOOL_MIN_VER="21"                                                           # Minimum version of the ESP tool expected.




####################################################################################################
# @brief            Queries the ESP tool for its version number.
# 
# @note             The actual value of the optional 'flatten' parameter is ignored. This function 
#                   simply checks to see if there is at least one parameter provided to it when 
#                   determining if the flag was set.
# 
# @param[in]        flatten                 [OPTIONAL] Flag indicating that all punctuation [e.g. 
#                                           periods] should be stripped from the version number. By 
#                                           convention, this should be set to 'true' or '1' if this 
#                                           feature is desired.
# 
# @retval           version-number          Returns the version number information of the ESP tool.
#                                           Presently, this is in the form of "major.minor".
####################################################################################################
function esptool_version()
{
    tmp=$(${PYTHON} ${ESP_TOOL} version | grep -v "v" | awk '{$1=$1}1')
    
    if [ $# -ge 1 ]; then
        echo "${tmp}" | sed -e 's/\.//g'
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Attempts to erase the platform over the designated COM port using the ESP tool.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           0                       Successfully erased the flash using the ESP tool.
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           >0                      Error code returned by the ESP tool. See the ESP tool's 
#                                           online documentation for details.
####################################################################################################
function esptool_erase_platform()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Erase flash using the ESP tool.
    ${PYTHON} ${ESP_TOOL} --port ${com} erase_flash
    
    echo $?
    
}




####################################################################################################
# @brief            Attempts to write the requested file to the platform over the designated COM 
#                   port using the ESP tool.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port. The exact form of each flash command varies based on 
#                   the target platform indicated.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# @param[in]        fpath                   Path to file (ideally absolute). This is the file to 
#                                           flash.
# 
# @retval           0                       Successfully flashed using the ESP tool.
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           -3                      File provided does not exist or has an invalid 
#                                           extension.
# @retval           -4                      Unsupported platform requested.
# @retval           >0                      Error code returned by the ESP tool. See the ESP tool's 
#                                           online documentation for details.
####################################################################################################
function esptool_flash_platform()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    declare fpath=${3}                                                          # File to flash. Ideally an absolute value.
    : ${fpath:=""}                                                              #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ "${fpath}" == "" ] || [ $# -gt 3 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    elif [[ "${fpath}" != *".bin" ]] || ! [ -f "${fpath}" ]; then
        echo -3
        return
    fi
    
    # Write the image provided to the ESP's flash. Variance based on platform mostly follows the 
    # online documentation for the ESP tool (see GitHub page for ESP tool project).
    case "${platform}" in
        ${PLATFORM_NODE_MCU_ESP8266})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 460800 write_flash --flash_mode dio --flash_size=detect 0x0000 "${fpath}"
            ;;
        ${PLATFORM_NODE_MCU_ESP32})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 460800 write_flash --flash_mode dio --flash_size=detect 0x1000 "${fpath}"
            ;;
        ${PLATFORM_NODE_MCPU_ESP8266} | ${PLATFORM_NODE_MCPU_ESP32})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 115200 write_flash --flash_size=detect "${fpath}"
            ;;
        *) echo -4 && return
    esac
    
    echo $?
    
}




####################################################################################################
# @brief            Attempts to read the entire flash of the requested platform over the designated 
#                   COM port using the ESP tool. The resulting binary is stored at the indicated 
#                   location.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port. The exact form of each read command varies based on 
#                   the target platform indicated.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# @param[in]        fpath                   Path to file (ideally absolute) that will be saved to 
#                                           [overwritten if it exists].
# 
# @retval           0                       Successfully read the flash using the ESP tool.
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           -3                      Output directory for file does not exists.
# @retval           -4                      Unable to determine a proper flash size to use for 
#                                           platform.
# @retval           -5                      Unsupported platform requested.
# @retval           >0                      Error code returned by the ESP tool. See the ESP tool's 
#                                           online documentation for details.
####################################################################################################
function esptool_clone_platform()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    declare fpath=${3}                                                          # File to save to. Ideally an absolute value.
    : ${fpath:=""}                                                              #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ "${fpath}" == "" ] || [ $# -gt 3 ]; then
        echo -1 && return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2 && return
    elif [ "${fpath}" == "" ] || ! [ -d $(dirname ${fpath}) ]; then
        echo -3 && return
    fi
    
    # Read in flash size and translate into bytes.
    raw_flash_size=$(esptool_read_flash_size ' ' "${com}")
    flash_size=$(echo "${raw_flash_size}" | tr '[:upper:]' '[:lower:]')
    if [[ "${flash_size}" == *"mb" ]]; then
       tmp=$(echo "${flash_size}" | sed -e 's/mb//g') 
       flash_size=$((${tmp}*1024*1024))
    elif [[ "${flash_size}" == *"kb" ]]; then
       tmp=$(echo "${flash_size}" | sed -e 's/kb//g') 
       flash_size=$((${tmp}*1024))
    elif [[ "${flash_size}" == *"b" ]]; then
       flash_size=$(echo "${flash_size}" | sed -e 's/b//g') 
    else
        echo -4 && return
    fi
    echo "Will attempt to read full flash size of ${raw_flash_size} (${flash_size} Bytes)."
    
    # Read the target platform's flash. The 'read_flash' command requires three positional arguments:
    # [starting-address], [flash-size], and [output-file]. Variance based on platform mostly follows 
    # the online documentation for the ESP tool (see GitHub page for ESP tool project).
    case "${platform}" in
        ${PLATFORM_NODE_MCU_ESP8266})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 460800 read_flash 0 ${flash_size} "${fpath}"
            ;;
        ${PLATFORM_NODE_MCU_ESP32})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 460800 read_flash 0 ${flash_size} "${fpath}"
            ;;
        ${PLATFORM_NODE_MCPU_ESP8266} | ${PLATFORM_NODE_MCPU_ESP32})
            ${PYTHON} ${ESP_TOOL} --port ${com} --baud 115200 read_flash 0 ${flash_size} "${fpath}"
            ;;
        *) echo -5 && return
    esac
    
    echo $?
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   MAC address value.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_mac_address()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # MAC data printed out when invoking the 'read_mac' command. Format:  "MAC: ##:##:##:##:##:##".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} read_mac | grep "MAC: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   chip identifier.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_chip_id()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Chip type printed out when invoking the 'chip_id' command. Format:  "Chip ID: 0x########".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} chip_id | grep "^Chip ID: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   chip type (ESP8266, ESP32, etc.).
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_chip_type()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Chip ID printed out when invoking most commands. Format:  "Chip is ESP...".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} chip_id | grep "^Chip is " | cut -d' ' -f 3- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   manufacturer information.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_manufacturer()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Manufacturer is printed out when invoking the 'flash_id' command. Format:  "Manufacturer: ...".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} flash_id | grep "^Manufacturer: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   flash type.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_flash_type()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Flash type is printed out when invoking the 'flash_id' command. Format:  "Device: ####".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} flash_id | grep "^Device: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   flash size.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_flash_size()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Flash size is printed out when invoking the 'flash_id' command. Format:  "Detected flash size: #...".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} flash_id | grep "^Detected flash size: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




####################################################################################################
# @brief            Uses the ESP tool to query the requested platform over the indicated COM for its 
#                   flash [register] status.
# 
# @details          Validates the input parameters are non-empty and then invokes the ESP tool using 
#                   the designated COM port.
# 
# @notes            At this time, the platform field is ignored. However, it is suspected this may 
#                   change in the future so trying to future proof things.
# 
# @param[in]        platform                Constant indicating platform to interact with. Should 
#                                           be equal to one of the defined "PLATOFRM_..." constants.
# @param[in]        COM                     COM port to connect to while utilizing the ESP tool.
# 
# @retval           -1                      One or more input parameters is invalid.
# @retval           -2                      Unsupported version of ESP tool installed.
# @retval           Unknown                 Unable to invoke the ESP tool or post-processing is 
#                                           incompatible with the current version of the ESP tool.
# @retval           [string]                Value read back from command.
####################################################################################################
function esptool_read_flash_status()
{
    # Process parameter(s) provided to script.
    declare platform=${1}                                                       # Form factor [platform] to interact with.
    : ${platform:=""}                                                           #  => IFF no parameter provided, set to ''.
    declare com=${2}                                                            # System COM port to use when communicating with ESP device.
    : ${com:=""}                                                                #  => IFF no parameter provided, set to ''.
    
    if [ "${platform}" == "" ] || [ "${com}" == "" ] || [ $# -gt 2 ]; then
        echo -1
        return
    elif [ $(esptool_version true) -lt ${ESP_TOOL_MIN_VER} ]; then
        echo -2
        return
    fi
    
    # Flash status is printed out when invoking the 'read_flash_status' command. Format:  "Status value: 0x####".
    tmp=$(${PYTHON} ${ESP_TOOL} --port ${com} read_flash_status | grep "^Status value: " | cut -d':' -f 2- | awk '{$1=$1}1')
    
    if [ "${tmp}" == "" ]; then
        echo "Unknown"
    else
        echo "${tmp}"
    fi
    
}




