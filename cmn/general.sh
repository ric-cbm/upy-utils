#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Library file providing BASH helper functions and constants available to all 
#                   MicroPython utility scripts.
# 
# @details          Provides general constants and helper functions that may be used across multiple 
#                   BASH helper functions.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - zenity
# 
# @version          1.0.0
####################################################################################################


####################################################################################################
# @details          Define file-specific constants.
# 
# @warning          While these constants are used in this file, take care to not be too generic in 
#                   your naming convention as all library files will be pulled into the active 
#                   envrionment when the 'lib.sh' file is sourced.
####################################################################################################
PYTHON="python3"                                                                # Define Python command [version] to use when invoking external Python scripts.

#DIR_MICROPYTHON_UTILS                                                          # Defined within <cmn>/lib.sh.
DIR_MICROPYTHON="$( cd "${DIR_MICROPYTHON_UTILS}/.." && pwd )"                  # Root directory for all of MicroPython related development.
DIR_BINARIES="${DIR_MICROPYTHON}/binaries"                                      # Root directory for binary outputs.
DIR_SCRIPTS="${DIR_MICROPYTHON}/scripts"                                        # Directory containing general scripts that will be pulled into each MicroPython build.
DIR_TARGETS="${DIR_MICROPYTHON}/targets"                                        # Directory containing subfolders where each is a build target.
DIR_SOURCE_ESP8266="${DIR_MICROPYTHON}/targets/esp8266"                         # Directory containing the source code repository used when building for the ESP8266.
DIR_SOURCE_ESP32="${DIR_MICROPYTHON}/targets/esp32"                             # Directory containing the source code repository used when building for the ESP32.
DIR_TARGET_ESP8266="${DIR_SOURCE_ESP8266}/ports/esp8266"                        # Directory containing the target-specific source code related to the ESP8266.
DIR_TARGET_ESP32="${DIR_SOURCE_ESP32}/ports/esp32"                              # Directory containing the target-specific source code related to the ESP32.
DIR_TOOLCHAINS="${DIR_MICROPYTHON}/toolchains"                                  # Directory containing all 3rd party toolchains, libraries, and SDKs.
DIR_TC_ESP_IDF="${DIR_TOOLCHAINS}/esp-idf"                                      # [ESP32]   Directory containing the 'Espressif IoT Development Framework' [ESP-IDF] toolchain.
DIR_TC_ESP_OPEN_SDK="${DIR_TOOLCHAINS}/esp-open-sdk"                            # [ESP8266] Directory containing the ESP open SDK project.
DIR_TC_XTENSA_ESP32="${DIR_TOOLCHAINS}/xtensa-esp32"                            # [ESP32]   Directory containing the ESP32 version of the SDK toolchain.
DIR_UTILS="${DIR_MICROPYTHON_UTILS}"                                            # Directory alias for 'DIR_MICROPYTHON_UTILS' that uses the more common name convention.




####################################################################################################
# @brief            Provides the logic to cleanly notify user of script termination and exit.
# 
# @details          Prints any provided message and exits with the provided exit code. When invoked 
#                   from a script NOT running in a terminal, prompts the user to hit the enter key
#                   to exit.
# 
# @note             Presumes any input via command line parameters means the parent script is 
#                   running in terminal.
# 
# @param[in]        exit_code               [Optional] Exit code to use when terminating script. 
#                                           When not provided, script exists with success [0].
# @param[in]        message                 [Optional] Message to print before exiting script. When 
#                                           not provided, no message is printed (no default message
#                                           presumed).
# 
# @returns          Prints 'message' to terminal and exits with provided 'exit_code'.
####################################################################################################
function exit_script()
{
    # Print any and all parameters starting at the second parameter provided to script.
    if [ $# -gt 1 ]; then
        echo "${@:2}"
    fi
    
    # Pause before exiting if the 'LS_COLORS' variable is not defined [presumes use case would only 
    # would occur within a terminal; not ideal, but 'good enough' for now].
    if ! [[ -v 'LS_COLORS' ]]; then
        echo ""
        read -p "Press enter to exit."
    fi
    
    # Exit with either the provided exit code or '0' to indicate success.
    if [ $# -ge 0 ]; then
        exit ${1}
    else
        exit 0
    fi
    
}




###################################################################################################
# @brief            Prompt a user for path to a file (or files).
# 
# @details          Uses the 'zenity' utility to prompt a user to select an input file.
# 
# @param[in]        allow-mutiple           Flag indicating if multiple items may be selected at 
#                                           once. Any value will suffice [function checks for 
#                                           parameter count >= 1 vs actual value for parameter].
# 
# @retval           path                    String containing path to file.
# @retval           path-list               If 'allow-multiple' is enabled and more than one 
#                                           response is selected by the user, the path list 
#                                           returned will be a single string with unique entry 
#                                           separated by '|'.
###################################################################################################
function prompt_for_file()
{
    if [ $# -ge 1 ]; then
        zenity --file-selection --multiple 2>/dev/null
    else
        zenity --file-selection 2>/dev/null
    fi
}




###################################################################################################
# @brief            Prompt a user for path to a directory (or directories).
# 
# @details          Uses the 'zenity' utility to prompt a user to select an input file.
# 
# @param[in]        allow-mutiple           Flag indicating if multiple items may be selected at 
#                                           once. Any value will suffice [function checks for 
#                                           parameter count >= 1 vs actual value for parameter].
# 
# @retval           path                    String containing path to file.
# @retval           path-list               If 'allow-multiple' is enabled and more than one 
#                                           response is selected by the user, the path list 
#                                           returned will be a single string with unique entry 
#                                           separated by '|'.
###################################################################################################
function prompt_for_directory()
{
    if [ $# -ge 1 ]; then
        zenity --file-selection --directory --multiple 2>/dev/null
    else
        zenity --file-selection --directory 2>/dev/null
    fi
}




###################################################################################################
# @brief            Prompts a user to make a yes or no selection until a valid selection is made. 
# 
# @details          Writes a prompt message to stdout and polls for response based on the mode 
#                   mode provided:
#                       - mode == 1 == Blank response == Yes
#                       - mode == 2 == Blank response == No
#                       - mode == 3 == Blank response not allowed
# 
#                   Responses are not not case sensitive ('nO' == 'No' == 'NO') and users may 
#                   optionally  pass just the letters 'y' or 'n' to indicate 'yes' or 'no'.
# 
# @param[in]        mode                    [OPTIONAL] Mode value of 1, 2, or 3. Invalid (or empty 
#                                           if no value provided) will be treated as a mode of '1'.
# @param[in]        message                 [OPTIONAL] Message to prompt user with before polling 
#                                           for a reply. Defaults to "Please enter in either yes
#                                           or no".
# 
# @retval           0                       The user selected "no" [false].
# @retval           1                       The user selected "yes" [true].
###################################################################################################
function prompt_for_yes_or_no()
{
    # Determine mode [see function header for mode details].
    if [ $# -ge 1 ] && [ "${1}" == 3 ]; then
        mode=3
        msg_suffix="[y|n]"
    elif [ $# -ge 1 ] && [ "${1}" == 2 ]; then
        mode=2
        msg_suffix="[y|N]"
    else
        mode=1
        msg_suffix="[Y|n]"
    fi
    
    # Structure message prompt.
    if [ $# -ge 2 ]; then
        tmp="${*:2}"
        if [[ "${tmp}" == *"?"* ]]; then
            msg="$(echo ${tmp} | sed -e 's/?//g' | awk '{$1=$1}1') ${msg_suffix}? "
        else
            msg="$(echo ${tmp} | sed -e 's/\.//g' -e 's/://g' | awk '{$1=$1}1') ${msg_suffix}: "
        fi
    else
        msg="Please enter in either yes or no ${msg_suffix}: "
    fi
    
    # Poll until a valid response is given by the user.
    while true; do 
        
        read -p "${msg}" response
        
        case "${response}" in
            "" )
                if [ "${mode}" == 1 ]; then
                    echo 1 && return
                elif [ "${mode}" == 2 ]; then
                    echo 0 && return
                fi
                ;;
            [Yy] | [Yy][Ee][Ss])
                echo 1 && return
                ;;
            [Nn] | [Nn][Oo])
                echo 0 && return
                ;;
            *)
        esac
    done
}




###################################################################################################
# @brief            Prints the warning message and informs the user the press the <ENTER> key to 
#                   continue processing. Acts as a processing delay to force user interaction and 
#                   acknowledgement.
# 
# @param[in]        warning-message         [OPTIONAL] Message to prompt user with before polling 
#                                           for the <ENTER> key. Defaults to "Please see message(s) 
#                                           above for details.".
###################################################################################################
function prompt_with_warning()
{
    # Structure warning message.
    if [ $# -ge 1 ]; then
        msg="[WARNING]> $@"
    else
        msg="Please see message(s) above for details."
    fi
    
    # Print warning message and wait for user input.
    echo "${msg}"
    read -p "Press enter to continue."
    
}




