#!/bin/bash
####################################################################################################
# @file
# 
# @details          Main script used to import all BASH library files available to MicroPython 
#                   utlity repository.
# 
# @author           Michael Stephens
# 
# @depends          
#                   - BASH 4.0++
# 
# @version          - 1.0.0
####################################################################################################


####################################################################################################
# @note             Determine root location [directory] for the MicroPython Utils install.
# 
# @warning          Make sure not to overwrite the value of 'DIR_CWD' that is likely set by the 
#                   calling script.
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	_DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$_DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
_DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
DIR_MICROPYTHON_UTILS="$( cd -P "${_DIR_CWD}/.." && pwd )"


####################################################################################################
# @brief            Source [load in] all bash scripts in library.
####################################################################################################
for fpath in $(find ${_DIR_CWD} -type f | grep "\.sh" | grep -v $(basename ${BASH_SOURCE[0]})); do
    source "${fpath}"
done


