#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Library file providing BASH helper functions and constants available to all 
#                   MicroPython utility scripts.
# 
# @details          
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
#                   - zenity
# 
# @version          1.0.0
####################################################################################################


####################################################################################################
# @details          Define file-specific constants.
# 
# @warning          While these constants are used in this file, take care to not be too generic in 
#                   your naming convention as all library files will be pulled into the active 
#                   envrionment when the 'lib.sh' file is sourced.
####################################################################################################
SCREEN_BAUD=115200                                                              # BAUD rate for all MicroPython instances should be 115200.
SCREEN_SCROLLBACK=200000                                                        # Provide an arbitrary large number of lines of scrollback for screen session.




####################################################################################################
# @brief            Kills any existing [detached] screen sessions and cleans up any hanging entries 
#                   within SockDir.
####################################################################################################
function screen_cleanup_sessions()
{
    for pid in $(screen -ls | grep 'Detached' | cut -d'.' -f 1 | awk '{$1=$1};1'); do
        kill -KILL ${pid}
    done
    sleep 0.5
    screen -wipe > /dev/null 2>&1
}




####################################################################################################
# @brief            Gather list of all [inactive in screen command] COM ports on system.
# 
# @details          Searches </dev> for any files beginning with 'ttyUSB' and returns them as a 
#                   space separated list.
# 
# @note             A COM port will be excluded if it is presently known to be associated with any 
#                   screen sessions. To mitigate this, consider running 'screen_cleanup_sessions()' 
#                   to remove inactive sessions before invoking this method.
# 
# @retval           list-of-COM-ports       Space separated list of all available com ports (in 
#                                           absolute path form). If no COMs are found, returns an 
#                                           empty string.
####################################################################################################
function comms_determine_avialable()
{
    comms=()
    for c in $(ls -L /dev | grep 'ttyUSB\|ttyACM'); do
        if [ $(screen -ls | grep "${c}" | wc -l) -eq 0 ]; then
            comms+=("/dev/${c}")
        fi
    done
    
    # Echo  the value determined above to the terminal to return the result to the caller.
    echo "${comms[@]}"
    
}




####################################################################################################
# @brief            Determines which COM port should be utilized. If more than one is available, 
#                   prompts the caller to request one.
# 
# @details          Invokes the helper methods 'screen_cleanup_sessions()' and 
#                   'comms_determine_available()' to gather a list of possible COM ports. If there 
#                   is more than one COM port found, prompts the user to select the appropriate COM 
#                   port using a 'select' loop.
# 
# @param[in]        descriptive             [OPTIONAL] Flag indicating that each COM port found 
#                                           should be queried to attempt to indicate the device type 
#                                           to the user. This will take some time and is disabled by 
#                                           default. By convention, this should be set to 'true' or  
#                                           '1' if this feature is desired.
# 
# @retval           COM-port                Absolute path to COM port that should be used. If empty, 
#                                           no available COM port found.
####################################################################################################
function comms_select_desired()
{
    # Kill any existing [detached] screen sessions before determining the list of available COM 
    # ports.
    screen_cleanup_sessions
    
    # Gather a list of all physical COM ports known [to the best of our ability] to not be in use.
    comms=($(comms_determine_avialable))
    
    # Determine COM port. If only one available, task completed. If not, ask user to make a 
    # selection from the available options.
    com=""
    if [ ${#comms[@]} -eq 1 ]; then
        com="${comms[0]}"
    elif [ ${#comms[@]} -gt 1 ]; then
        
        # IFF descriptive flag requested, attempt to determine device type.
        if [ $# -ge 1 ]; then
            annotated=()
            for c in "${comms[@]}"; do
                tmp_platform=($(platform_determine_supported ${c}))
                if [ "${tmp_platform}" != "" ] && [ ${#tmp_platform[@]} -eq 1 ]; then
                    annotated+=("${c} [${tmp_platform[0]}]")
                else
                    annotated+=("${c} [Unrecognized Device]")
                fi
            done
            comms=("${annotated[@]}")
        fi
        
        # Prompt user for COM port to connect to if more than one available.
        select c in "${comms[@]}"; do
            if [ "${c}" != "" ]; then
                com="${c}"
                break
            fi
        done
    fi
    
    # Echo the value determined above to the terminal to return the result to the caller.
    echo "${com}"
    
}




