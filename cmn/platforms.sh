#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Library file providing BASH helper functions and constants available to all 
#                   MicroPython utility scripts.
# 
# @details          Defines supported platforms and functions used to determine which platform to 
#                   interact with in a script.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
#                   - zenity
# 
# @version          1.0.0
####################################################################################################


####################################################################################################
# @details          Define file-specific constants.
# 
# @warning          While these constants are used in this file, take care to not be too generic in 
#                   your naming convention as all library files will be pulled into the active 
#                   envrionment when the 'lib.sh' file is sourced.
####################################################################################################
PLATFORM_NODE_MCU_ESP8266="NodeMCU--ESP8266"                                    # Platform type for NodeMCU with the ESP8266 chip.
PLATFORM_NODE_MCU_ESP32="NodeMCU--ESP32"                                        # Platform type for NodeMCU with the ESP32 chip.
PLATFORM_GENERAL_ESP8266="General--ESP8266"                                     # Platform type for general ESP8266 chip.
PLATFORM_GENERAL_ESP32="General--ESP32"                                         # Platform type for general ESP32 chip.




####################################################################################################
# @brief            Gather list of [relevant] platforms. 
# 
# @details          Searches local environment for any variables beginning with "PLATFORM_". If a 
#                   'COM-port' value is provided, the results are then filtered according to
#                   information queried from the target.
# 
# @note             More work could be done in the future to utilize type when providing back 
#                   'generic' options. Presently not felt to be worth the extra processing effort.
# 
# @param[in]        COM-port                Absolute path to COM port. If empty, will not attempt to 
#                                           preselect the appropriate platform (no COM port value 
#                                           presumed).
# 
# @retval           list-of-platforms       Space separated list of all relevant platforms. If no 
#                                           platforms are found, returns an empty string.
####################################################################################################
function platform_determine_supported()
{
    # Assume current script is loaded into the environment [was sourced] and extract all entries 
    # that start with "PLATFORM_" as valid options for platforms.
    platforms=( $( ( set -o posix ; set ) | grep "^PLATFORM_" | cut -d'=' -f2 ) )
    
    # Attempt to filter out irrelevant options if a COM port value was provided.
    if [ $# -eq 1 ]; then
        
        # Query platform information [use placeholder character for platform type].
        p_type=$(esptool_read_chip_type     " " "${1}")
        p_mfr=$(esptool_read_manufacturer   " " "${1}")
        
        tmp=()
        generic=()
        for p in "${platforms[@]}"; do
            # Each name entry should be in the form:  [manufacturer]--...--[type].
            mfr=$(echo "${p}" | cut -d'-' -f 1)
            typ="${p##*-}"
            
            # Filter by type and then manufacturer. Make sure to save generic options for 
            # reference later if no manufacturer matches found.
            if [[ "${mfr}" == "General" ]]; then
                generic+=("${p}")
            elif [[ "${p_type}" == *"${typ}"* ]]; then
                if      [ "${p_mfr}" == "ef" ] && [ "${mfr}" == "NodeMCU" ] \
                    ||  [ "${p_mfr}" == "c8" ] && [ "${mfr}" == "NodeMCU" ]
                then
                    tmp+=("${p}")
                fi
            fi
        done
        
        # Override known platforms variable IFF at least one match was found.
        if [ ${#tmp[@]} -ge 1 ]; then
            platforms=(${tmp[@]})
        # If no specific platform value(s) found, provide generic platform values only.
        else
            platforms=(${generic[@]})
        fi
    fi
    
    # Echo  the value determined above to the terminal to return the result to the caller.
    echo "${platforms[@]}"
    
}




####################################################################################################
# @brief            Determines which platform type should be utilized. If more than type is 
#                   available, prompts the caller to request one.
# 
# @details          Invokes the helper method 'platform_determine_supported()' to gather a list of 
#                   possible platforms. If there is more than one platform found, prompts the user 
#                   to select the appropriate platform using a 'select' loop.
# 
# @param[in]        COM-port                Absolute path to COM port. If empty, will not attempt to 
#                                           preselect the appropriate platform (no COM port value 
#                                           presumed).
# 
# @retval           platform                Platform type auto-determined or selected by the user.
####################################################################################################
function platform_select_target()
{
    # Gather a list of all [relevant] platforms.
    platforms=($(platform_determine_supported "${1}"))
    
    # Determine platform. If only one available, task completed. If not, ask user to make a 
    # selection from the available options.
    platform=""
    if [ ${#platforms[@]} -eq 1 ]; then
        platform="${platforms[0]}"
    elif [ ${#platforms[@]} -gt 1 ]; then
        
        # Prompt user for platform to use if more than one is available.
        select p in "${platforms[@]}"; do
            if [ "${p}" != "" ]; then
                platform="${p}"
                break
            fi
        done
    fi
    
    # Echo the value determined above to the terminal to return the result to the caller.
    echo "${platform}"
    
}




