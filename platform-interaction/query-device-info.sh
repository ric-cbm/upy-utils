#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Simple script to query all general identifier information for a target platform.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
# 
# @version          1.0.0
# 
# @retval           0                       Terminal launched successfully.
# @retval           1                       Could not locate an available COM port to utilize.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################

# Determine com port to utilize when interacting with the ESP flash.
com=$(comms_select_desired)
if [ "${com}" == "" ]; then
    exit_script 1 "No COM ports available to connect to (make sure to close down any serial port connections before running script)."
fi

# Query and print out summary information for platform:
# >> NOTE:  No platform information is required at this time so leaving out (passing single-space 
#           [' '] character for parameter 1).
echo "Chip Type:     $(esptool_read_chip_type       ' ' "${com}")"
echo "Chip ID:       $(esptool_read_chip_id         ' ' "${com}")"
echo "Manufacturer:  $(esptool_read_manufacturer    ' ' "${com}")"
echo "Flash Type:    $(esptool_read_flash_type      ' ' "${com}")"
echo "Flash Size:    $(esptool_read_flash_size      ' ' "${com}")"
echo "Flash Status:  $(esptool_read_flash_status    ' ' "${com}")"
echo "MAC Address:   $(esptool_read_mac_address     ' ' "${com}")"




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0




