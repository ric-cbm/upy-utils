#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Simple script to kill any active sessions, prompt user for available terminals, 
#                   and launch terminal with desired baud rate.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
# 
# @version          1.0.0
# 
# @retval           0                       Terminal launched successfully.
# @retval           1                       Could not locate an available COM port to utilize.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################
# Kill any existing [detached] screen sessions to enable re-connecting [prefer this method over 
# reconnect based on end-user case]. Note:  This is a bit redundant as 'comms_select_desired()' does 
# this as well. However, trying to future proof as this is a core requirement for this script to 
# work.
screen_cleanup_sessions

# Launch a new screen session.
com=$(comms_select_desired)
if [ "${com}" != "" ]; then
    screen -S $(basename ${com}) ${com} ${SCREEN_BAUD} -h ${SCREEN_SCROLLBACK}
else
    exit_script 1 "No COM ports available to connect to. Exiting."
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "Task(s) completed without errors."




