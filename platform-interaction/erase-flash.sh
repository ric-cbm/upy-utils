#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Script handles erasing and flashing a ESP board using the "esptool.py" script.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - Python    [v3]
#                   - ESPTool   [v2.1++] [https://github.com/espressif/esptool]
# 
# @version          1.0.0
# 
# @retval           0                       Terminal launched successfully.
# @retval           1                       Could not locate an available COM port to utilize.
# @retval           2                       Could not determine platform target.
# @retval           3                       Unable to erase flash.
# @retval           4                       Unable to write to flash.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################

# Determine com port to utilize when interacting with the ESP flash.
com=$(comms_select_desired)
if [ "${com}" == "" ]; then
    exit_script 1 "No COM ports available to connect to (make sure to close down any serial port connections before running script)."
fi

# Determine which platform we are using.
platform="$(platform_select_target "${com}")"
if [ "${platform}" == "" ]; then
    exit_script 2 "Could not determine target platform."
fi

# Prompt user to erase the flash until they explicitly agree or disagree. Execute according to 
# [valid] response value.
echo "Erasing flash memory on target platform ..."
output=$(esptool_erase_platform "${platform}" "${com}")
printf "%s\n"  "${output[@]}" | head -n -1
rc=$(echo "${output[@]}" | tail -1)
if [ ${rc} -eq -2 ]; then
    exit_script 3  "[FATAL-ERROR]> ESP tool unsupported or is before version ${ESP_TOOL_MIN_VER}."
elif ! [ ${rc} -eq 0 ]; then
    exit_script 3 "[FATAL-ERROR]> Error code [${rc}] received from the ESP tool while attempting to erasing the flash."
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "Task(s) completed without errors."




