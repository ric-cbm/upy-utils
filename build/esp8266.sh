#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Compiles the current downloaded version of the MicroPython project for the 
#                   ESP8266 platform.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - make
# 
# @version          1.1.0
# 
# @retval           0                       Successfully updated the ESP open SDK utility.
# @retval           1                       The 'make' command for the Micropython build raised an 
#                                           error.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################
# Update path to include ESP open SDK.
export PATH="${DIR_TC_ESP_OPEN_SDK}/xtensa-lx106-elf/bin:${PATH}"

# Change to ports directory for relevant platform.
cd ${DIR_TARGET_ESP8266}

# Compile for target.
make 
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 1 "Error building MicroPython binary [rc == ${rc}]."
else
    echo ""
    echo "Successfully built target '$(basename $(pwd))'."
fi

# [If possible] Copy over result to output folder.
final_binary="$(pwd)/build/firmware-combined.bin"
if [ -e ${final_binary} ]; then
    
    dest_dir="${DIR_BINARIES}/$(basename $(pwd))"
    if ! [ -d "${dest_dir}" ]; then
        mkdir -p "${dest_dir}"
    fi
    
    echo "Final build image will be copied to <${dest_dir}>."
    
    # Attempt to use version number in name.
    tmp_fpath_ver_info="$(pwd)/build/genhdr/mpversion.h"
    if [ -e "${tmp_fpath_ver_info}" ]; then
        fname="$(cat "${tmp_fpath_ver_info}" | grep -i "micropy_version_string" | awk '{$1=$1}1' | cut -d' ' -f 3 | sed -e 's/"//g' -e "s/'//g" | awk '{$1=$1}1')"
    else
        fname="$(basename ${final_binary} | cut -d'.' -f1)"
    fi
    
    # Check to see if timestamp should be used in file name.
    resp=$(prompt_for_yes_or_no 2 "Would you like a timestamp appended to the file name?")
    if [ "${resp}" == "1" ]; then
        fname="${fname}--$(date +%Y%m%d-%H%M%S)"
    fi
    
    # Copy over file to destination.
    cp "${final_binary}" "${dest_dir}/${fname}.bin"
    
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "All build tasks completed successfully."


