#!/bin/bash
####################################################################################################
# @file
# 
# @brief            Script pulls and compiles the latest version of the 'esp-open-sdk' toolchain.
# 
# @note             To prevent quirks, the existing toolchain will be cleaned before the Git 
#                   repository is updated. To mitigate this, the remote [origin] is ping'd to ensure 
#                   it is up before wiping out the existing install using 'make clean'.
# 
# @author           Michael Stephens
# 
# @depends
#                   - BASH      [4.0++]
#                   - make
#                   - git
# 
# @version          1.0.0
# 
# @retval           0                       Successfully updated the ESP open SDK utility.
# @retval           1                       Unable to access remote repository [nothing modified].
# @retval           2                       Encountered error while running 'make clean'.
# @retval           3                       Error with one or more of the Git update commands.
# @retval           4                       Encountered error while running 'make'.
####################################################################################################


####################################################################################################
# @note             Elevate to terminal window so able to prompt user for input easily [we could 
#                   launch the final screen command via 'gnome-terminal', but prompting for user 
#                   input more complicated].
####################################################################################################
if ! [ -t 1 ]; then
    gnome-terminal -e "/bin/bash ${BASH_SOURCE[0]}"
    exit $?
fi


####################################################################################################
# @note             Determine location [directory] where the present script resides and source 
#                   library file(s).
####################################################################################################
# Resolve '$__src' until the file is no longer a symlink.
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
	# Grab next component of __src 
	DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
	__src="$(readlink "$__src")"
	
	# If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
	[[ $__src != /* ]] && __src="$DIR_CWD/$__src"
done

# Define base script directory and root directory for installation.
DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"

# Load in library functions available to all MicroPython utility scripts.
source ${DIR_CWD}/../cmn/lib.sh




####################################################################################################
# @note             Start of core processing for script begins here.
####################################################################################################
cd ${DIR_TC_ESP_OPEN_SDK}

# Ensure the remote is available before proceeding.
git ls-remote > /dev/null 2>&1
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 1 "Remote origin for repository is down. Exiting without modifying toolchain."
fi

# Cleanup before pulling to avoid quirks.
make clean
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 2 "Encountered unknown error [${rc}] while running 'make clean' on the existing build of the ESP open SDK toolchain."
fi

# Update and synchronize git repository.
git pull
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 3 "Encountered unknown error [${rc}] while pulling in the latest firmware for the Git repository."
fi
#### 
git submodule sync
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 3 "Encountered unknown error [${rc}] while synchronizing git submodules."
fi
#### 
git submodule update --init
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 3 "Encountered unknown error [${rc}] while updating Git submodules.."
fi


# Compile updated toolchain.
make
rc=$?
if ! [ ${rc} -eq 0 ]; then
    exit_script 4 "Encountered unknown error [${rc}] while building the ESP open SDK toolchain."
fi




####################################################################################################
# @note             Exit script without errors, but do so by invoking helper method to ensure 
#                   consistent script termination.
####################################################################################################
exit_script 0 "Successfully updated the ESP open SDK toolchain."




